variable "compartment_id" {
  description = "(Required) (Updatable) The OCID of the compartment."
  type        = string
  default     = ""
}

variable "instance_display_name" {
  description = "A user-friendly name. Does not have to be unique, and it's changeable."
  type        = string
  default     = ""
}

variable "availability_domain" {
  description = "(Required) The availability domain of the instance."
  type        = string
  default     = ""
}

variable "is_management_disabled" {
  description = "(Optional) (Updatable) Whether the agent running on the instance can run all the available management plugins."
  type        = bool
  default     = false
}

variable "is_monitoring_disabled" {
  description = "(Optional) (Updatable) Whether the agent running on the instance can gather performance metrics and monitor the instance"
  type        = bool
  default     = false
}

variable "boot_volume_type" {
  description = "(Optional) Emulation type for volume."
  type        = string
  default     = "PARAVIRTUALIZED"
}

variable "firmware" {
  description = "(Optional) Firmware used to boot VM."
  type        = string
  default     = "UEFI_64"
}

variable "is_consistent_volume_naming_enabled" {
  description = "(Optional) Whether to enable consistent volume naming feature."
  type        = bool
  default     = true
}

variable "is_pv_encryption_in_transit_enabled" {
  description = "(Optional) Whether to enable in-transit encryption for the boot volume's paravirtualized attachment. "
  type        = bool
  default     = true
}

variable "network_type" {
  description = "(Optional) Emulation type for the physical network interface card (NIC)."
  type        = string
  default     = "PARAVIRTUALIZED"
}

variable "remote_data_volume_type" {
  description = "(Optional) Emulation type for volume"
  type        = string
  default     = "PARAVIRTUALIZED"
}

variable "extended_metadata" {
  description = "(Optional) (Updatable) Additional metadata key/value pairs that you provide. They serve the same purpose and functionality as fields in the 'metadata' object."
  type        = map(string)
  default     = {}
}

variable "fault_domain" {
  description = "(Optional) A fault domain is a grouping of hardware and infrastructure within an availability domain."
  type        = string
  default     = null
}

variable "ipxe_script" {
  description = "The iPXE script which to continue the boot process on the instance."
  type        = string
  default     = null
}

variable "preserve_boot_volume" {
  description = "(Optional) Specifies whether to delete or preserve the boot volume when terminating an instance."
  type        = bool
  default     = false
}

variable "boot_volume_size_in_gbs" {
  description = "The size of the boot volume in GBs. Minimum value is 50 GB and maximum value is 16384 GB (16TB)."
  type        = number
  default     = "50"
}

variable "kms_key_id" {
  description = "The OCID of the Key Management key to assign as the master encryption key for the boot volume."
  type        = string
  default     = null
}

variable "shape" {
  # https://docs.cloud.oracle.com/en-us/iaas/Content/Compute/References/computeshapes.htm
  description = "(Required) (Updatable) The shape of an instance. The shape determines the number of CPUs, amount of memory, and other resources allocated to the instance."
  type        = string
  default     = ""
}

variable "assign_public_ip" {
  description = "(Optional) (Updatable) Whether the VNIC should be assigned a public IP address."
  type        = bool
  default     = true
}

variable "defined_tags" {
  description = "(Optional) (Updatable) Defined tags for this resource. Each key is predefined and scoped to a namespace"
  type        = map(string)
  default     = {}
}

variable "freeform_tags" {
  description = "(Optional) (Updatable) Free-form tags for this resource. Each tag is a simple key-value pair with no predefined name, type, or namespace."
  type        = map(any)
  default     = {}
}

variable "vnic_name" {
  description = "A user-friendly name for the VNIC."
  type        = string
  default     = ""
}

variable "hostname_label" {
  description = "(Optional) (Updatable) The hostname for the VNIC's primary private IP. Used for DNS."
  type        = string
  default     = ""
}

variable "nsg_ids" {
  description = "(Optional) (Updatable) A list of the OCIDs of the network security groups (NSGs) to add the VNIC to."
  type        = list(string)
  default     = []
}

variable "private_ips" {
  description = "(Optional) A private IP address of your choice to assign to the VNIC. Must be an available IP address within the subnet's CIDR. If you don't specify a value, Oracle automatically assigns a private IP address from the subnet. "
  type        = list(string)
  default     = []
}

variable "skip_source_dest_check" {
  description = "(Optional) (Updatable) Whether the source/destination check is disabled on the VNIC."
  type        = bool
  default     = false
}

variable "subnet_id" {
  description = "(Required) The OCID of the subnet to create the VNIC in."
  type        = string
  default     = ""
}

variable "dedicated_vm_host_id" {
  description = "(Optional) The OCID of dedicated VM host."
  type        = string
  default     = null
}

variable "ssh_authorized_keys" {
  description = "(Optional) Public SSH keys path to be included in the ~/.ssh/authorized_keys file for the default user on the instance."
  type        = string
  default     = null
}

variable "user_data" {
  description = "(Optional) Provide your own base64-encoded data to be used by Cloud-Init to run custom scripts or provide custom Cloud-Init configuration. "
  type        = string
  default     = null
}

variable "source_id" {
  # https://docs.cloud.oracle.com/en-us/iaas/images/
  description = "(Required) The OCID of an image or a boot volume to use."
  type        = string
  default     = ""
}

variable "source_type" {
  description = "(Optional) The source type for the instance."
  type        = string
  default     = "image"
}

variable "instance_timeout" {
  description = "(Optional) Timeout setting for creating instance. "
  default     = "25m"
}

variable "block_volume" {
  description = "(Optional) Map of block volumes to be created and attached to instance."
  type        = any
  default     = {}
}

variable "backup_policy" {
  description = "(Optional) Backup policy id to assign a volumes"
  type = object({
    boot_volume  = string
    block_volume = map(string)
  })
  default = {
    boot_volume  = ""
    block_volume = {}
  }
}
