module "instance_web-server" {
  source                              = "git::https://gitlab.com/ganex-cloud/terraform/terraform-oci-compute-intance.git?ref=master"
  compartment_id                      = module.compartiment.compartment_id
  instance_display_name               = "web-server"
  source_id                           = "ocid1.image.oc1.iad.aaaaaaaaqpkxp5dj6mbe2fuwzmpl5n5szeubzyxveez6kqpxlg5ej2awbhqa"
  shape                               = "VM.Standard2.1"
  nsg_ids                             = [module.nsg_web-servers.nsg_id]
  availability_domain                 = module.vcn.availability_domains[0]
  subnet_id                           = module.vcn.public_subnet_id[0]
  ssh_authorized_keys                 = "ssh-rsa AAAA...."
  is_pv_encryption_in_transit_enabled = true
  block_volume = {
    volume1 = {
      display_name = "srv"
      size_in_gbs  = 50
    }
  }
  backup_policy = {
    boot_volume = module.backup_policy-default.backup_policy_id
    block_volume = {
      volume1 = module.backup_policy-default.backup_policy_id
    }
  }
}
