locals {
  defaults = {
    block_volume = {
      defined_tags    = {}
      display_name    = null
      freeform_tags   = {}
      kms_key_id      = null
      size_in_gbs     = 55
      source_details  = []
      attachment_type = "paravirtualized"
    }
  }
}
