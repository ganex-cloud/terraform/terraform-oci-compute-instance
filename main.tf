resource "oci_core_instance" "this" {
  availability_domain = var.availability_domain
  compartment_id      = var.compartment_id
  shape               = var.shape

  agent_config {
    is_management_disabled = var.is_management_disabled
    is_monitoring_disabled = var.is_monitoring_disabled
  }

  create_vnic_details {
    subnet_id              = var.subnet_id
    assign_public_ip       = var.assign_public_ip
    defined_tags           = var.defined_tags
    display_name           = var.vnic_name
    freeform_tags          = var.freeform_tags
    hostname_label         = var.hostname_label
    nsg_ids                = var.nsg_ids
    private_ip             = element(concat(var.private_ips, list("")), length(var.private_ips) == 0 ? 0 : 1)
    skip_source_dest_check = var.skip_source_dest_check
  }

  dedicated_vm_host_id                = var.dedicated_vm_host_id
  defined_tags                        = var.defined_tags
  display_name                        = var.instance_display_name
  extended_metadata                   = var.extended_metadata
  fault_domain                        = var.fault_domain
  freeform_tags                       = var.freeform_tags
  ipxe_script                         = var.ipxe_script
  is_pv_encryption_in_transit_enabled = var.is_pv_encryption_in_transit_enabled

  launch_options {
    boot_volume_type                    = var.boot_volume_type
    firmware                            = var.firmware
    is_consistent_volume_naming_enabled = var.is_consistent_volume_naming_enabled
    is_pv_encryption_in_transit_enabled = var.is_pv_encryption_in_transit_enabled
    network_type                        = var.network_type
    remote_data_volume_type             = var.remote_data_volume_type
  }

  metadata = {
    ssh_authorized_keys = var.ssh_authorized_keys
    user_data           = var.user_data
  }

  source_details {
    source_id               = var.source_id
    source_type             = var.source_type
    boot_volume_size_in_gbs = var.boot_volume_size_in_gbs
    kms_key_id              = var.kms_key_id
  }

  preserve_boot_volume = var.preserve_boot_volume

  timeouts {
    create = var.instance_timeout
  }
}

resource "oci_core_volume" "this" {
  for_each            = var.block_volume
  availability_domain = var.availability_domain
  compartment_id      = var.compartment_id
  defined_tags        = lookup(each.value, "defined_tags", local.defaults.block_volume.defined_tags)
  display_name        = lookup(each.value, "display_name", local.defaults.block_volume.display_name)
  freeform_tags       = lookup(each.value, "freeform_tags", local.defaults.block_volume.freeform_tags)
  kms_key_id          = lookup(each.value, "kms_key_id", local.defaults.block_volume.kms_key_id)
  size_in_gbs         = lookup(each.value, "size_in_gbs", local.defaults.block_volume.size_in_gbs)

  dynamic "source_details" {
    for_each = lookup(each.value, "source_details", local.defaults.block_volume.source_details)
    content {
      id   = lookup(source_details.value, "id")
      type = lookup(source_details.value, "type")
    }
  }
}

resource "oci_core_volume_attachment" "this" {
  for_each                            = var.block_volume
  attachment_type                     = lookup(each.value, "attachment_type", local.defaults.block_volume.attachment_type)
  instance_id                         = oci_core_instance.this.*.id[0]
  is_pv_encryption_in_transit_enabled = var.is_pv_encryption_in_transit_enabled
  volume_id                           = oci_core_volume.this[each.key].id
}

resource "oci_core_volume_backup_policy_assignment" "boot_volume" {
  count     = length(var.backup_policy.boot_volume) == 0 ? 0 : 1
  asset_id  = oci_core_instance.this.*.boot_volume_id[0]
  policy_id = var.backup_policy.boot_volume
}

resource "oci_core_volume_backup_policy_assignment" "block_volume" {
  for_each  = length(keys(var.backup_policy.block_volume)) == 0 ? {} : var.backup_policy.block_volume
  asset_id  = oci_core_volume.this[each.key].id
  policy_id = each.value
}
